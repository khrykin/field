
var Plot = {
	init: function ( settings ) {
		Plot.config = {
			$placeholder: "#plotPlaceholder",
			$scale: 1000000000
		};
		$.extend( Plot.config, settings );
		Plot.setup();

	},
	setup: function () {

		var elements = [];

		var Graph = [];

		$('.element').each(function () {

			var inputs = $(this).find('input[type = "text"]');

			var x = $(inputs[0]).val();

			elements.push({
				position: {
					x: parseFloat($(inputs[0]).val()),
					y: parseFloat($(inputs[1]).val()),
				},
				radius: parseFloat($(inputs[2]).val()),
				value: parseFloat($(inputs[3]).val()),
			});
		});

		switch ($('input[name = "plotOptions"]:checked').val()) {
					case "Z" :
						var Z = parseFloat($('#InputPlotZ').val());

						for (x = 0; x <= $(Plot.config.$placeholder).height(); x +=1 ) {
							var B = Field.multiLoop(elements, { x: x , y: Z });
							Graph.push([ x , Plot.config.$scale * Math.sqrt( B.x* B.x + B.y * B.y)]);
						}

					break
					case "X" :
						var X = parseFloat($('#InputPlotX').val());

						for (y = 0; y <= $(Plot.config.$placeholder).height(); y +=1 ) {
							var B = Field.multiLoop(elements, { x: X , y: y });
							Graph.push([ x , Plot.config.$scale * Math.sqrt( B.x* B.x + B.y * B.y)]);
						}
					break
		}

		/*for ( x = element.position.x - element.radius + 1 ; x < element.position.x + element.radius; x += 1) {
			var B = Field.multiLoop(elements, { x: x , y: element.position.y - 100  });
			M1.push([x, 1000000000 * Math.sqrt( B.x * B.x + B.y * B.y )]);
		}*/

		plot = $.plot(Plot.config.$placeholder, [

				{ data: Graph, label: "B(" + $('input[name = "plotOptions"]:checked').val()  + ")  = -0.00" },
			], {
				series: {
					lines: {
						show: true
					}
				},
				crosshair: {
					mode: "x"
				},
				grid: {
					hoverable: true,
					autoHighlight: false
				},
				/*yaxis: {
					min: -1.2,
					max: 1.2
				}*/
			});

		var legends = $(Plot.config.$placeholder + " .legendLabel");

		legends.each(function () {
			// fix the widths so they don't jump around
			$(this).css('width', $(this).width());
		});

		var updateLegendTimeout = null;
		var latestPosition = null;

		function updateLegend() {

			updateLegendTimeout = null;

			var pos = latestPosition;

			var axes = plot.getAxes();
			if (pos.x < axes.xaxis.min || pos.x > axes.xaxis.max ||
				pos.y < axes.yaxis.min || pos.y > axes.yaxis.max) {
				return;
			}

			var i, j, dataset = plot.getData();
			for (i = 0; i < dataset.length; ++i) {

				var series = dataset[i];

				// Find the nearest points, x-wise

				for (j = 0; j < series.data.length; ++j) {
					if (series.data[j][0] > pos.x) {
						break;
					}
				}

				// Now Interpolate

				var y,
					p1 = series.data[j - 1],
					p2 = series.data[j];

				if (p1 == null) {
					y = p2[1];
				} else if (p2 == null) {
					y = p1[1];
				} else {
					y = p1[1] + (p2[1] - p1[1]) * (pos.x - p1[0]) / (p2[0] - p1[0]);
				}

				legends.eq(i).text(series.label.replace(/=.*/, "= " + y.toFixed(4)));
			}
		}

		$(Plot.config.$placeholder).on("plothover",  function (event, pos, item) {
			latestPosition = pos;
			if (!updateLegendTimeout) {
				updateLegendTimeout = setTimeout(updateLegend, 50);
			}
		});

	}
}	