
var elementsNum = 0;

var Elliptic  = {

		K : function ( k ) {
			if ( k > 1 || k < 0 ) {
				throw "Argument k = " + k + " is  out of allowable range ([0,1])"
			} else {
				var sum = 1, term = 1, above = 1, below = 2;

				for ( j = 1; j <= 100; j++) {
					term *= above / below;
					sum += Math.pow( k, j ) * Math.pow( term, 2 );
					above += 2;
					below += 2;
				}
				sum *= 0.5 * Math.PI;
				return sum;
			}
		},

		E: function ( k ) {
			if ( k > 1 || k < 0 ) {
				throw "Argument k = " + k + " is  out of allowable range ([0,1])"
			} else {
				var sum = 1, term = 1, above = 1, below = 2;

				for ( j = 1; j <= 100; j++) {
					term *= above / below;
					sum -= Math.pow( k, j ) * Math.pow( term, 2 ) / above;
					above += 2;
					below += 2;
				}
				sum *= 0.5 * Math.PI;
				return sum;
			}
		}
}

var Interface  = {
	init: function ( settings ) {
			Interface.config = {
				$elements: {

				}
			};
			$.extend( Interface.config, settings );
			Interface.setup();

			Interface.switchHash(window.location.hash);

			$(window).on('hashchange', function () {
				Interface.switchHash( window.location.hash );
				Plot.init();
			});

	},
	setup: function () {

		$('form').submit(function ( event ) {
			event.preventDefault();
		});

		$('<form></form>').appendTo('.elements');
		
		$.validate({
			form : '.add-new-element',
			            
			modules : 'security',

			validateOnBlur : false,

			onError : function() {

			},
			onValidate : function() {

			},

			onSuccess : function() {

				elementsNum++;

				var newElementForm = this.form;

				var formHTML = $('.new-element-form-inner').html();
				$('<form></form>').appendTo('.elements')

				.attr('class', 'form-horizontal element')
				.attr('role', 'form')
				.prepend( function () {
					
					var inputsHTML = '<button type="button" class="close element-delete" aria-hidden="true">×</button>';

					$(newElementForm + ' input[type = "text"]').each(function(){
					 	inputsHTML += ' \
					 	<div class="form-group"> \
			                <label for="Element' + elementsNum + $( this ).attr('id')  + '" class="col-sm-2 control-label">\
			                ' + $('label[for = "' + $( this ).attr('id') + '"]').html() + ' \
			                </label> \
			                <div class="col-sm-10"> \
			                  <input \
				                type="text" \
				                class="form-control" \
				                id="Element' + elementsNum + $( this ).attr('id')  + '" \
				                placeholder="' + $( this ).attr('placeholder') + '" \
				                value = "' + $( this ).val() + '" \
				                > \
			                </div> \
		              	</div> \
              			';
					});

					return inputsHTML + '<hr>';
				});
				;
				$(this.form)[0].reset();
			},

			language : { badInt: 'Только числа с плавающей точкой'}
	    }); 

		$.validate({
			form: '#picProperties',
			language : { badInt: 'Только числа с плавающей точкой'},
			onSuccess : function() {
				var settings = {
					$elements: [
					/*{
						position: { 
							x: 350, 
							y: 300 
						},
						value: 10,
						radius: 100
					},*/
					],
					$precision: $('#inputStep').val(),
					$numberOfSteps: $('#inputStepNum').val(),
				};

				$('.element').each(function () {

					var inputs =  $(this).find('input[type = "text"]');

					var x = $(inputs[0]).val();

					settings.$elements.push({
						position: {
							x: parseFloat($(inputs[0]).val()),
							y: parseFloat($(inputs[1]).val()),
						},
						radius: parseFloat($(inputs[2]).val()),
						value: parseFloat($(inputs[3]).val()),
					});
				});
				Field.init( settings );
			}
		});

		$('input[name = "plotOptions"]').change( function () {
			//alert($('input[name = "plotOptions"]:checked').val());

			switch ($('input[name = "plotOptions"]:checked').val()) {
					case "Z" :
						$('#InputPlotZ')
		                    .attr ('data-validation' , 'number')
		                    .attr ('data-validation-allowing' , 'float')
						;
						$('#InputPlotX')
		                    .removeAttr ('data-validation')
		                    .removeAttr ('data-validation-allowing')
		                   	.parent().removeClass('has-success has-error')
						;
					break
					case "X" :
						$('#InputPlotX')
		                    .attr ('data-validation' , 'number')
		                    .attr ('data-validation-allowing' , 'float')
						;
						$('#InputPlotZ')
		                    .removeAttr ('data-validation')
		                    .removeAttr ('data-validation-allowing')
		                   	.parent().removeClass('has-success has-error')
						;
					break
				}

		});

		$.validate({
			form: '#plotProperties',
			
			validateOnBlur : false,

			onValidate : function() {
				//alert($('input[name = "plotOptions"]:checked').val());

			},
			onSuccess : function() {
				Plot.init();
			},

			language : { badInt: 'Только числа с плавающей точкой'}
		});


		$( document ).on('click', '.element-delete',  function () {
			$(this).parent().remove();
		});

	},
	switchHash: function ( hash ) {
		switch (window.location.hash) {
		case "#pic" :
		  $('#plot').hide();
		  $('#pic').show();

		  $('#picTab').attr("class","active");
		  $('#plotTab').removeAttr('class');

		break
		case "#plot" :
		  $('#pic').hide();
		  $('#plot').show();

		  $('#plotTab').attr("class","active");
		  $('#picTab').removeAttr('class');

		break
		}
	}

}

/*
var Plot = {
	init: function() {
		alert('Plotted smth');
	}
}*/

//For Debug Purposes
$('<pre>alpha \t k \n</pre>').appendTo('#landfill');

var Field = {
	init: function ( settings ) {
			Field.config = {
				$container: $('#picture'),
				$canvas: {
					height: "500",
					width: "750"
				},
				$precision: 10 ,
				$numberOfSteps: 100,
				$elements: [
					
				],
				$superc: [
					{
						position: {
							x: 300,
							y: 200
						},
						width: 200 ,
						height: 10
					},
				],

				$elementsProperties: {
					stroke: 'none',
					r : 10,
					//'strokeWidth': '',
					//'fill' : 'red'
				}
			};
			$.extend( Field.config, settings );
			Field.svg =  Field.config.$container.find('svg');
			Field.config.$container.html('');
			Field.setup();
			Field.config.$container.html( function () { return this.innerHTML });
	},

	mu0: 1.26 * 0.000001,

	setup: function () {
		Field.buildCanvas();
		Field.drawMultiLoop( Field.config.$elements, Field.config.$numberOfSteps );
		Field.buildElements();
		//alert( Field.singleLoop(Field.config.$elements[0], {x: 250, y: 250}).x + " : " + Field.singleLoop(Field.config.$elements[0], {x: 250, y: 250}).y );
		//for (y = 110; y <= 300; y += 10) {
		//Field.drawMagMultiLine(Field.config.$elements, Field.config.$elements[0], {x: y, y: 200}, 100);
		//Field.drawMagMultiLine(Field.config.$elements, Field.config.$elements[0], {x: y, y: 200}, 100, 'plus'); }
	},



	draw: function ( str , color ) {
		$('<path></path>').appendTo( Field.config.$container.find('svg') ).attr({
			d : str,
			fill : 'none',
			stroke: color
		});

		Field.config.$container.html( function() { return this.innerHTML });

	},
	buildCanvas: function () {
		$('<svg version="1.1" xmlns="http://www.w3.org/2000/svg"></svg>')
		.appendTo(Field.config.$container)
		.css ({ 'overflow': 'hidden',
		 		'position': 'relative'
		 	})
		.attr({
				'height' : Field.config.$canvas.height,
				'width' : Field.config.$canvas.width
			})
		;
	},

	buildElements: function () {
		$.each( Field.config.$elements, function ( element, properties) {
			$('<circle/>')
				.appendTo( Field.svg.selector )
				.attr({
					'cx': properties.position.x ,
					'cy': properties.position.y ,
					'r':  Field.config.$elementsProperties.r ,
					'stroke': Field.config.$elementsProperties.stroke ,
					'stroke-width': Field.config.$elementsProperties.strokeWidth ,
					'fill': properties.value > 0 ? 'red' : 'blue'
				})
			;
		});
		$.each(Field.config.$superc, function ( element, properties) {
			var svgpos = {};
			svgpos.x = properties.position.x - properties.width / 2;
			svgpos.y = properties.position.y - properties.height / 2;
			/*$('<rect/>')
				.appendTo( Field.svg.selector )
				.attr({
					'x' : svgpos.x,
					'y' : svgpos.y,
					'width' : properties.width,
					'height' : properties.height,
				});*/
		});
	},

	singleCharge: function ( element , where ) {
		var x = where.x - element.position.x;
		var y = where.y - element.position.y;
		return {
			x: (element.value > 0 ? 1 : -1) * element.value * element.value * x / (Math.pow( x*x + y*y , 3/2)),
			y: (element.value > 0 ? 1 : -1) * element.value * element.value * y / (Math.pow( x*x + y*y , 3/2)),
		}
	},

	singleLoop: function ( element , where ) {
		var x = where.x - element.position.x;
		var y = where.y - element.position.y;

		var r = Math.abs(x);

		var B0 = element.value * 2 * Field.mu0 / ( 2 * element.radius );
		
		var alpha = r / element.radius;
		var beta  = y / element.radius;
		var gamma = y / r;

		var Q = (1 + alpha) * (1 + alpha) + beta * beta;
		
		//For debug purposes
		//alert ("a: " + alpha + "b: " + beta + "Q:  " + Q  );
		
		var k = 4 * alpha / Q;
		
		//For debug purposes
		//$('#landfill pre').append(alpha + "\t" + k + "\t" + Q + '\n');
		
		var K = Elliptic.K(k);
		var E = Elliptic.E(k);

		var By = 
					B0 / ( Math.PI * Math.sqrt( Q ) ) * 
					(
						E 

						* 
						
						(1 - alpha * alpha - beta * beta )/
						
						(Q - 4 *alpha)
						
						+
						
						K
					);
		var Br = 
					B0 * gamma / ( Math.PI * Math.sqrt( Q ) ) * 
					(
						E 

						* 
						
						(1 + alpha * alpha + beta * beta )/
						
						(Q - 4 *alpha)
						
						-
						
						K
					);		
		return {
			x: isNaN(Br) ? 0 : x >= 0 ? Br : -Br ,
			y: isNaN(By) ? 0 : By
		}

	},

	multiLoop: function (elements, where ) {
		var B = {x: 0, y: 0}

		$.each(elements, function ( elementNumber, element )  {
				B.x += Field.singleLoop(element, where).x;
				B.y += Field.singleLoop(element, where).y;
		});

		//var superc = Field.config.$superc[0];

		/*if ( 
			(
				where.x <= superc.position.x + superc.width / 2 &&
				where.x >= superc.position.x - superc.width / 2
			)
			&&
			(
				where.y <= superc.position.y + superc.height/2&&
				where.y >= superc.position.y - superc.height/2
			)
		) { 
			B = { x: 0, y: 0}
		} */
		return B
	},

	drawMagMultiLine: function ( elements, element, beginPoint, steps, type ) {
		var delta = {
			x: 0,
			y: 0
		};
		var l = Field.config.$precision;
		var B = {}
		var where = beginPoint;
		var str  = "M" + beginPoint.x + " " + beginPoint.y;


		for ( i = 0 ; i <= steps; i++ ) {

			B = Field.multiLoop( elements, where );
			B.abs = Math.sqrt( B.x * B.x + B.y * B.y );
			delta.x =  l * B.x / B.abs;
			delta.y =  l * B.y / B.abs;

			where.x += (type == 'plus' ? 1 : -1) * delta.x;
			where.y += (type == 'plus' ? 1 : -1) *  delta.y;

			str += "L" + where.x + " " + where.y;

		}

		Field.draw( str, element.value > 0 ? 'red' : 'blue' ); 
	},

	drawMultiCharge: function ( elements, steps ) {
		var epsilon = 1;
		var beginPoint = {};	

		$.each(elements, function (elementNumber, element) {
			for (alpha = 0; alpha <= 2*Math.PI ; alpha += Math.PI / Math.abs(element.value)) {
				
				beginPoint.x = element.position.x + epsilon * Math.cos( alpha );
				beginPoint.y = element.position.y + epsilon * Math.sin( alpha );

				Field.drawMultiLine( elements, element, beginPoint, steps );
			}
		});
	},

	multiCharge: function ( elements , where ) {
		
		var E = { x: 0, y: 0};

		$.each(elements , function ( elementNumber, element ){
							
				E.x += Field.singleCharge( element, where ).x;
				E.y += Field.singleCharge( element, where ).y;

		});
		return E
	},

	drawMultiLine: function ( elements, element, beginPoint, steps ) {
		var delta = {
			x: 0,
			y: 0
		};
		var l = Field.config.$precision;
		var E = {}
		var where = beginPoint;
		var str  = "M" + beginPoint.x + " " + beginPoint.y;

		for ( i = 0 ; i <= steps; i++ ) {
			
			E = Field.multiCharge( elements, where );
			E.abs = Math.sqrt( E.x * E.x + E.y * E.y );
			delta.x =  l * E.x / E.abs;
			delta.y =  l * E.y / E.abs;

			where.x += (element.value > 0 ? 1 : -1) * delta.x;
			where.y += (element.value > 0 ? 1 : -1) * delta.y;

			str += "L" + where.x + " " + where.y;

		}

		Field.draw( str, element.value > 0 ? 'red' : 'blue' ); 
	},


	drawMultiLoop: function ( elements, steps ) {
		
		var epsilon = 5;
		var delta = 1;
		var beginPoint = {};

		$.each(elements, function (elementNumber, element) {

			var R = element.radius;

			for ( var x = element.position.x - R + epsilon ; x < element.position.x + R; x += 2* epsilon ) {
				
				beginPoint.y = element.position.y;
				beginPoint.x = x;

				Field.drawMagMultiLine( elements, element, beginPoint, steps, 'plus' );
			}

			for ( var x = element.position.x - R + epsilon ; x < element.position.x + R; x +=2 * epsilon ) {
				
				beginPoint.y = element.position.y;
				beginPoint.x = x;

				Field.drawMagMultiLine( elements, element, beginPoint, steps);
			}

		});
	},
}

$( document ).ready( Interface.init );